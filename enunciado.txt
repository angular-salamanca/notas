Esto es una app de Angular que ya tiene la maquetación HTML/CSS montada y la separación en módulos y componentes hecha.
En la raíz del proyecto tienes un esquema visual de cada componente (componentes.png) y aquí tienes la correspondencia
entre módulos y componentes:

CoreModule: AulaComponent y CabeceraComponent
SharedModule: LoadingComponent
AlumnosModule: ListaComponent
EstadisticasModule: EstadisticasAulaComponent

1. En la carpeta raíz tienes un JSON con algunos alumnos de ejemplo para que veas qué forma tienen. Declara una interfaz para
controlar la forma de cada alumno (si crees que en la aplicación los alumnos tendrán una forma diferente a la que tienen en el JSON,
crea dos interfaces). Recuerda que cada interfaz va en un archivo.
1. Crea un servicio en la carpeta core que almacenará el listado de alumnos con sus notas y se encargará de todas las
operaciones sobre el listado.
2. Este servicio deberá contener un BehaviorSubject que emitirá un nuevo array de alumnos cada vez que la lista sufra algún cambio:

public alumnos$ = new BehaviorSubject<Alumno[]>([]);  // Creamos un nuevo BehaviorSubject que emitirá objetos con forma
                                                      // de array de alumnos y que emite como valor inicial un array vacío

3. Cada componente que necesite obtener el array de alumnos deberá incluir el servicio como dependencia (es decir, pedírselo a la
inyección de dependencias) y suscribirse al BehaviorSubject. Cada vez que éste emita un nuevo array de alumnos, el componente
deberá almacenarlo en una propiedad propia (a la que la vista se bindeará) que tendrá como tipo un array de alumnos.
4. Cada aula debe filtrar el listado de alumnos que recibe. Piensa si debes usar el método filter() de los arrays o el operador
filter() de RxJS.
5. Para probar el sistema, en el constructor del servicio haz que el BehaviorSubject emita un array con dos alumnos (si quieres
cógelos del JSON y conviértelos aquí: https://json-to-js.com/). Imprime por consola el array desde los componentes que
están escuchando.
6. Levanta la API REST de alumnos con npm start (desde la carpeta de la API). La URL base es https://localhost:3000/alumnos/
7. Crea un servicio en la carpeta core que tendrá los métodos de conexión a la API. Empieza creando un método para obtener
todos los alumnos (hay que apuntar con GET a la URL base). Recuerda que el método tiene que devolver el observable.
8. En el servicio de alumnos, crea un método para cargar todos los alumnos. Este método será llamado desde el propio constructor
y se encargará de llamar al método correspondiente del servicio API. Cuando obtenga una respuesta, debe hacer que su BehaviorSubject
emita el array de alumnos obtenido.
9. Una vez comprobado que los componentes están recibiendo los alumnos que vienen de la API, realiza todos los bindings y
la lógica necesaria para que funcionen los listados y las estadísticas. Fíjate que en las dos primeras estadísticas de cada componente
hay una cara sonriente y verde si la nota es mayor o igual a 5, y una cara triste y roja si la nota es menor o igual a 5. En el
componente tienes comentarios explicando qué clases CSS tienes que quitar o poner.
