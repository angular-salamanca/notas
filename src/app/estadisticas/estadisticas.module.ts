import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EstadisticasAulaComponent } from './estadisticas-aula/estadisticas-aula.component';

@NgModule({
  declarations: [EstadisticasAulaComponent],
  imports: [
    CommonModule
  ],
  exports: [EstadisticasAulaComponent]
})
export class EstadisticasModule { }
